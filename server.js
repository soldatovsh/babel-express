import express from 'express';
const app = express();

// https://github.com/developit/express-es6-rest-api

app.get('/', (req, res)=>{
  res.send('Hello World!');
});

app.listen(3000, ()=>{
  console.log('Example app listening on port 3000!')
});